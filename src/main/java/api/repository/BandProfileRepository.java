package api.repository;

import api.models.bandprofile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BandProfileRepository extends JpaRepository<bandprofile, Long> {
}
