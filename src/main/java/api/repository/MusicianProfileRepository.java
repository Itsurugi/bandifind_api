package api.repository;

import api.models.musicianprofile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MusicianProfileRepository extends JpaRepository<musicianprofile, Long> {

}
