package api.repository;

import api.models.instruments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstrumentsRepository extends JpaRepository<instruments, Long> {
}
