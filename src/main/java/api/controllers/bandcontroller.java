package api.controllers;

import api.models.bandprofile;
import api.repository.BandProfileRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/bands")
public class bandcontroller {
    @Autowired
    BandProfileRepository bandprofilerepo;

    @GetMapping("")
    public @ResponseBody
    ResponseEntity getAllProfiles(){
        List<bandprofile> genrelist;
        try{
            genrelist = bandprofilerepo.findAll();
            if(genrelist.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(genrelist, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
