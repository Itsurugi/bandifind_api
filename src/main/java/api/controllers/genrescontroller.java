package api.controllers;

import api.models.genres;
import api.repository.GenresRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/genres")
public class genrescontroller {
    @Autowired
    GenresRepository genresrepo;
    private Gson gson = new Gson();

    @GetMapping("")
    public @ResponseBody
    ResponseEntity getAllProfiles(){
        List<genres> genrelist;
        try{
            genrelist = genresrepo.findAll();
            if(genrelist.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(genrelist, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
