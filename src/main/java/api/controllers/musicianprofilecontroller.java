package api.controllers;

import api.models.instruments;
import api.models.musicianprofile;
import api.repository.MusicianProfileRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Properties;

@CrossOrigin
@RestController
@RequestMapping("/musicianprofile")
public class musicianprofilecontroller {
    @Autowired
    MusicianProfileRepository musicianrepo;
    private Gson gson = new Gson();

    @GetMapping("")
    public @ResponseBody
    ResponseEntity getAllProfiles(){
        List<musicianprofile> profiles;
        try{
            profiles = musicianrepo.findAll();
            if(profiles.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(profiles, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{profileid}")
    public @ResponseBody
    ResponseEntity getProfile(@PathVariable Long profileid){
        musicianprofile profiles;
        try{
            if(musicianrepo.findById(profileid).isPresent()){
                profiles = musicianrepo.findById(profileid).get();
                return new ResponseEntity<>(profiles, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public @ResponseBody ResponseEntity createProfile(@RequestBody String campaign){
        try {
            musicianprofile profile = gson.fromJson(campaign, musicianprofile.class);

            musicianrepo.save(profile);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
