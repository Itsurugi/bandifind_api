package api.controllers;

import api.models.instruments;
import api.repository.InstrumentsRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/instruments")
public class instrumentscontroller {
    @Autowired
    InstrumentsRepository instrumentsrepo;
    private Gson gson = new Gson();

    @GetMapping("")
    public @ResponseBody
    ResponseEntity getAllProfiles(){
        List<instruments> genrelist;
        try{
            genrelist = instrumentsrepo.findAll();
            if(genrelist.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(genrelist, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
