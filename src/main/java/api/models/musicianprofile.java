package api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "musicianprofile")
@EntityListeners(AuditingEntityListener.class)
public class musicianprofile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("stagename")
    private String stagename;

    @JsonProperty("location")
    private String location;

    @JsonProperty("phonenumber")
    private String phonenumber;

    @JsonProperty("age")
    private Integer age;

    @JsonProperty("expierence")
    private String expierence;

    @ManyToMany
    @JoinTable(
            name = "musician_instruments",
            joinColumns = {
                    @JoinColumn(name = "musicianprofile_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "instrument_id", nullable = false, updatable = false)
            }
    )
    @JsonProperty("id")
    protected Set<instruments> instruments = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "musician_genres",
            joinColumns = {
                    @JoinColumn(name = "musicianprofile_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "genre_id", nullable = false, updatable = false)
            }
    )
    @JsonProperty("id")
    protected Set<genres> genres = new HashSet<>();

    @OneToOne(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "gear_id")
    @JsonProperty("id")
    private gear gear_id;
}
