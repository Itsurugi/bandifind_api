package api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "bandalbum")
public class bandalbum {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @JsonProperty("src")
    private String src;

    @JsonProperty("title")
    private String title;

    @JsonProperty("releaseyear")
    private String releaseyear;

    @JsonProperty("recordlabel")
    private String recordlabel;

    @ManyToMany
    @JoinTable(
            name = "album_genres",
            joinColumns = {
                    @JoinColumn(name = "album_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "genre_id", nullable = false, updatable = false)
            }
    )
    @JsonProperty("genres")
    protected Set<genres> genres = new HashSet<>();
}
