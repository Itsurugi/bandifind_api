package api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "bandprofile")
@EntityListeners(AuditingEntityListener.class)
public class bandprofile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("hometown")
    private String hometown;

    @JsonProperty("country")
    private String country;

    @ManyToMany
    @JoinTable(
            name = "band_genres",
            joinColumns = {
                    @JoinColumn(name = "band_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "genre_id", nullable = false, updatable = false)
            }
    )
    @JsonProperty("genres")
    protected Set<genres> genres = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "band_videos",
            joinColumns = {
                    @JoinColumn(name = "band_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "video_id", nullable = false, updatable = false)
            }
    )
    @JsonProperty("videos")
    protected Set<bandvideos> videos = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "band_photos",
            joinColumns = {
                    @JoinColumn(name = "band_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "photo_id", nullable = false, updatable = false)
            }
    )
    @JsonProperty("photos")
    protected Set<bandphoto> photos = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "band_position",
            joinColumns = {
                    @JoinColumn(name = "band_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "position_id", nullable = false, updatable = false)
            }
    )
    @JsonProperty("position")
    protected Set<bandposition> position = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "band_albums",
            joinColumns = {
                    @JoinColumn(name = "band_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "album_id", nullable = false, updatable = false)
            }
    )
    @JsonProperty("albums")
    protected Set<bandalbum> albums = new HashSet<>();
}
