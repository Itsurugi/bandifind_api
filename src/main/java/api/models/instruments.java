package api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "instruments")
@EntityListeners(AuditingEntityListener.class)
public class instruments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("src")
    private String src;
//    @ManyToMany(mappedBy = "instruments", fetch = FetchType.EAGER)
//    protected Set<musicianprofile> musicianprofiles = new HashSet<>();
}
